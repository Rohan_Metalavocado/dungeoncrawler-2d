﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatureSystems;

public class EnemyController : MonoBehaviour
{
    public CreatureType CreatureType = CreatureType.Slime;
    public CreatureStatModifier StatModifier;
    public CreatureAttackTiming AttackTiming;

    [HideInInspector]
    public Stats EnemyStats;
    [HideInInspector]
    public Rigidbody2D Rigidbody2D;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void TakeDamage (int _damage)
    {
        int defence = Mathf.RoundToInt(StatModifier.Defence * EnemyStats.BaseStatTotal);

        EnemyStats.CurrentHealth -= _damage;
        Mathf.Clamp(EnemyStats.CurrentHealth, 0, EnemyStats.TotalHealth);


    }
}
