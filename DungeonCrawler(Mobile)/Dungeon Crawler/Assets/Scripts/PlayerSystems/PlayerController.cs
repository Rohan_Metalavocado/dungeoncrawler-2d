﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatureSystems;

/// <summary>
/// Handles all player inputs and calls the required player functions.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    MovementController movementController;
    CombatController combatController;

    public CreatureType CreatureType = CreatureType.Slime;

    [HideInInspector]
    public Stats PlayerStats;
    [HideInInspector]
    public Rigidbody2D Rigidbody2D;

    public int debug_EXPtoADD = 5;
    public bool IsFeeding = false;

    /// <summary>
    /// A list which stores the values for all creatures.
    /// These values are set in the GameController in the inspector,
    /// then passed to the player as this list.
    /// </summary>
    public List<Creature> CreatureList;

	// Use this for initialization
	void Awake ()
    {
        MakeSingleton();

        ControlScheme.SetControlScheme(ControlScheme.ControlSchemeType.Default);

        //Finding instances of required player components
        PlayerStats = GetComponent<Stats>();
        Rigidbody2D = GetComponent<Rigidbody2D>();
        movementController = FindObjectOfType<MovementController>();
        combatController = FindObjectOfType<CombatController>();

        //Get the creature values from the GameController
        CreatureList = GameController.Instance.Creatures;
    }

    private void Start()
    {
        PlayerStats.TotalHealth = Mathf.CeilToInt(PlayerStats.BaseStatTotal * CreatureList[(int)CreatureType].StatModifier.Health);
        PlayerStats.CurrentHealth = PlayerStats.TotalHealth;
    }

    /// <summary>
    /// Checks to see if a PlayerController already exists, to ensure that there's a single PlayerController instance existing at a time.
    /// </summary>
    void MakeSingleton ()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

	// Update is called once per frame
	void Update ()
    {
        CheckMovement();
        CheckAttacking();
        CheckFeeding();

        if (Input.GetKey(KeyCode.Alpha1)) PlayerStats.AddExperience(debug_EXPtoADD);
    }

    void CheckAttacking()
    {
        if (Input.GetKey(ControlScheme.Attack))
        {
            combatController.OnAttack();
        }
    }

    void CheckFeeding ()
    {
        if (!IsFeeding && Input.GetKey(ControlScheme.Feed))
        {
            IsFeeding = true;
        }
        else
        {
            IsFeeding = false;
        }
    }

    public void EatFood (int _healthGain, int _expGain)
    {
        PlayerStats.AddHealth(_healthGain);
        PlayerStats.AddExperience(_expGain);
    }

    void CheckMovement ()
    {
        Vector2 moveInputVector = Vector2.zero;

        //Check if the player has any of the movement keys pressed.
        //Multiple key presses will be added to create the resultant movement direction.
        if (Input.GetKey(ControlScheme.MoveNorth)) moveInputVector += new Vector2(0, 1);
        if (Input.GetKey(ControlScheme.MoveSouth)) moveInputVector += new Vector2(0, -1);
        if (Input.GetKey(ControlScheme.MoveEast))  moveInputVector += new Vector2(1, 0);
        if (Input.GetKey(ControlScheme.MoveWest))  moveInputVector += new Vector2(-1, 0);

        //Send moveInputVector.normalized to MovementController
        //The vector is normalised to ensure uniform movement speed in all 8 possible directions
        movementController.MovePlayer(moveInputVector.normalized);
    }
}

/// <summary>
/// The player's control scheme. Currently set to a default scheme.
/// TODO: May include the ability to modify the control scheme in future.
/// </summary>
public static class ControlScheme
{
    public enum ControlSchemeType { Default };

    public static KeyCode MoveNorth;
    public static KeyCode MoveSouth;
    public static KeyCode MoveEast;
    public static KeyCode MoveWest;
    public static KeyCode Attack;
    public static KeyCode Feed;


    /// <summary>
    /// Sets the player's control scheme using a switch to determine which _scheme to use.
    /// </summary>
    /// <param name="_scheme"></param>
    public static void SetControlScheme(ControlSchemeType _scheme)
    {
        switch (_scheme)
        {
            case ControlSchemeType.Default:
                MoveNorth = KeyCode.W;
                MoveSouth = KeyCode.S;
                MoveEast = KeyCode.D;
                MoveWest = KeyCode.A;

                Attack = KeyCode.R;
                Feed = KeyCode.Space;
                break;
        }
    }
}