﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatureSystems;

/// <summary>
/// Controls the Player's combat functionalities.
/// </summary>
public class CombatController : MonoBehaviour
{
    /// <summary>
    /// The collider used to detect if enemies have been attacked.
    /// This is updated when the player character evolves.
    /// </summary>
    public Collider2D AttackCollider;

    private List<GameObject> hitInThisAttack;

    PlayerController player;
    private bool canAttack = true;

    // Use this for initialization
    void Start()
    {
        player = PlayerController.Instance;
        hitInThisAttack = new List<GameObject>();

        if (AttackCollider == null)
        {
            Debug.LogError("No attack collider on: " + gameObject.name);
            return;
        }

        if (!AttackCollider.isTrigger)
        {
            Debug.LogWarning("Attack collider on -" + gameObject.name + "- has been changed to a trigger");
            AttackCollider.isTrigger = true;
        }
    }

    public void OnAttack()
    {
        if (!canAttack) return;

        ////Update the AttackTiming before doing the attack routine.
        ////This will allow for multiple attacks to be supported (passing an additional parameter to CreatureAttackTiming which specifies attack number/type)
        //player.AttackTiming = new CreatureAttackTiming(player.CreatureType);

        switch (player.CreatureType)
        {
            case CreatureType.Slime:
                StartCoroutine(AttackRoutine());
                break;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.isTrigger) return;

        //hit
        Debug.Log("Hit " + collision.gameObject.name);
        
        if (!hitInThisAttack.Contains(collision.gameObject))
        {
            hitInThisAttack.Add(collision.gameObject);
            Debug.Log("Added " + collision.gameObject.name + " to hitInThisAttack");

            EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();

            if (enemy)
            {
                enemy.TakeDamage(Mathf.RoundToInt(player.CreatureList[(int)player.CreatureType].StatModifier.Attack * player.PlayerStats.BaseStatTotal));
            }
        }
    }

    IEnumerator AttackRoutine ()
    {
        canAttack = false;
        yield return new WaitForSeconds(player.CreatureList[(int)player.CreatureType].AttackTiming.Delay);

        AttackCollider.enabled = true;
        yield return new WaitForSeconds(player.CreatureList[(int)player.CreatureType].AttackTiming.Duration);

        hitInThisAttack.Clear();
        AttackCollider.enabled = false;
        yield return new WaitForSeconds(player.CreatureList[(int)player.CreatureType].AttackTiming.Cooldown);

        canAttack = true;
        yield return null;
    }
}
