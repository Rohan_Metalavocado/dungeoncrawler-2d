﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatureSystems;

/// <summary>
/// Controls the movement of the player's character
/// </summary>
public class MovementController : MonoBehaviour
{
    //The reference to the player
    PlayerController player;

    //The movement values for the latestCreatureType. Will be updated when the creature type changes.
    CreatureStatModifier statModifier;
    CreatureMovement creatureMovement;

    //The last known creature type. Used to decrease assignments of movement values.
    CreatureType latestCreatureType;

    //If the player isn't moving
    bool atRest = true;

    //If the player can move (Co-routine isn't currently handling motion)
    bool canMove = true;

    //The current movement direction vector
    Vector2 movementInput;

    private void Start()
    {
        //Set initial movement values from the player's instance
        player = PlayerController.Instance;
        latestCreatureType = player.CreatureType;
        UpdateMovementValues();
    }

    /// <summary>
    /// Given a normalised movement direction vector, will move the player's character using MovementSpeed to scale the speed of movement.
    /// </summary>
    /// <param name="_movementInput"></param>
    public void MovePlayer (Vector2 _movementInput)
    {
        //If no movement input is set, cancel the co-routine if it is running and exit this method.
        if (_movementInput.magnitude == 0)
        {
            StopCoroutine(MovementRoutine());
            movementInput = Vector2.zero;
            atRest = true;
            canMove = true;
            return;
        }
        else
        {
            movementInput = _movementInput;
        }

        //If the player's creature has changed, refresh the movement values.
        if (player.CreatureType != latestCreatureType)
        {
            latestCreatureType = player.CreatureType;
            UpdateMovementValues();
        }

        switch (player.CreatureType)
        {
            case CreatureType.Slime:
                //Placeholder testing movement code
                //Replace with Slime-movement when implemented//
                if (canMove)
                {
                    StartCoroutine(MovementRoutine());
                }
                break;
        }
    }

    /// <summary>
    /// Update the values used to calculate the player's movement. 
    /// Only call when the type has been changed to increase efficiency.
    /// </summary>
    void UpdateMovementValues ()
    {
        statModifier = player.CreatureList[(int)player.CreatureType].StatModifier;
        creatureMovement = player.CreatureList[(int)player.CreatureType].Movement;
    }

    IEnumerator MovementRoutine()
    {
        //Player can't start another movement co-routine
        canMove = false;

        if (atRest)
        {
            //Wait for the inertia delay to finish
            yield return new WaitForSeconds(player.CreatureList[(int)player.CreatureType].Movement.InertiaDelay);
        }

        //Player is now in motion
        atRest = false;

        //Move each frame according to the movementCurve
        ////Look into if changing velocity to the curve will break with framerate
        ////Look into whether position-changing would be more reliable.

        player.Rigidbody2D.velocity = movementInput * player.CreatureList[(int)player.CreatureType].StatModifier.MoveSpeed;
        yield return new WaitForSeconds(player.CreatureList[(int)player.CreatureType].Movement.Duration);

        //Wait for the cooldown to expire
        yield return new WaitForSeconds(player.CreatureList[(int)player.CreatureType].Movement.Cooldown);

        //Player can now start another movement co-routine
        canMove = true;
        yield return null;
    }
}
