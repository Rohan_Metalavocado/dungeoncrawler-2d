﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreatureSystems;

public class GameController : MonoBehaviour
{
    public List<Creature> Creatures = new List<Creature>();


    public static GameController Instance;

    private void Awake()
    {
        if (Instance && Instance != this)
        {
            Debug.LogWarning("More than one GameController was found in the scene, deleting additional instance.");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
}
