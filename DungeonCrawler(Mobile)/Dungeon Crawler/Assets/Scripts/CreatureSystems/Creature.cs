﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CreatureSystems
{
    public enum CreatureType
    {
        Slime
    }

    [System.Serializable]
    public class Creature : System.Object
    {
        public CreatureType Type;
        public CreatureStatModifier StatModifier;
        public CreatureAttackTiming AttackTiming;
        public CreatureMovement Movement;
        public CreatureDrops Drops;
    }

    [System.Serializable]
    public class CreatureStatModifier : System.Object
    {
        [Range(0.0f, 1.0f)]
        public float Health = 0.0f;
        [Range(0.0f, 1.0f)]
        public float Attack = 0.0f;
        [Range(0.0f, 1.0f)]
        public float Defence = 0.0f;
        public float MoveSpeed = 0.0f;

        /// <summary>
        /// Returns a StatModifier with values corresponding to the given CreatureType
        /// </summary>
        /// <param name="_creatureType"></param>
        /// <returns></returns>
        public CreatureStatModifier (CreatureType _creatureType)
        {
            switch (_creatureType)
            {
                case CreatureType.Slime:
                    SetModifiers(0.5f, 0.1f, 0.4f, 5.0f);
                    break;
                default:
                    Debug.LogError("Invalid CreatureType given, no StatModifier set for: (" + _creatureType.ToString() + ")");
                    SetModifiers(0.0f, 0.0f, 0.0f, 0.0f);
                    break;
            }
        }

        /// <summary>
        /// Set the modifier values for the StatModifier. 
        /// </summary>
        /// <param name="_health"></param>
        /// <param name="_attack"></param>
        /// <param name="_defence"></param>
        /// <param name="_moveSpeed"></param>
        void SetModifiers(float _health, float _attack, float _defence, float _moveSpeed)
        {
            //Made into a Vector3 and normalised just to be certain that the total isn't greater than 1.
            float sum = _health + _attack + _defence;
            if (sum > 1.0f) Debug.LogWarning("Might want to check StatModifier values; greater than 1.0f: " + sum);
            
            //Assign the normalised values to their respective modifiers.
            //They are divided by the sum to ensure the total doesn't exceed 1.0f
            Health = _health / sum;
            Attack = _attack / sum;
            Defence = _defence / sum;
            MoveSpeed = _moveSpeed; //MoveSpeed is special in that it isn't scaled by level or BaseStats, but is based on CreatureType
        }
    }

    [System.Serializable]
    public class CreatureAttackTiming : System.Object
    {
        public float Delay = 0.0f;
        public float Duration = 0.0f;
        public float Cooldown = 0.0f;

        /// <summary>
        /// Returns a AttackTiming with values corresponding to the given CreatureType
        /// </summary>
        /// <param name="_creatureType"></param>
        /// <returns></returns>
        public CreatureAttackTiming(CreatureType _creatureType)
        {
            switch (_creatureType)
            {
                case CreatureType.Slime:
                    Delay = 0.1f;
                    Duration = 0.5f;
                    Cooldown = 1.0f;
                    break;
                default:
                    Debug.LogError("Invalid CreatureType given, no AttackTiming set for: (" + _creatureType.ToString() + ")");

                    Delay = 0.0f;
                    Duration = 0.0f;
                    Cooldown = 0.0f;
                    break;
            }
        }
    }

    [System.Serializable]
    public class CreatureMovement : System.Object
    {
        public float InertiaDelay = 0.0f;
        public float Duration = 0.0f;
        public float Cooldown = 0.0f;

        //Use animation curve?
        public AnimationCurve movementCurve;

        /// <summary>
        /// Returns a CreatureMovement with values corresponding to the given CreatureType
        /// </summary>
        /// <param name="_creatureType"></param>
        /// <returns></returns>
        public CreatureMovement(CreatureType _creatureType)
        {
            switch (_creatureType)
            {
                case CreatureType.Slime:
                    InertiaDelay = 0.3f;
                    Duration = 0.5f;
                    Cooldown = 0.3f;
                    break;
                default:
                    Debug.LogError("Invalid CreatureType given, no CreatureMovement set for: (" + _creatureType.ToString() + ")");

                    InertiaDelay = 0.0f;
                    Duration = 0.0f;
                    Cooldown = 0.0f;
                    break;
            }
        }
    }

    [System.Serializable]
    public class CreatureDrops : System.Object
    {
        public int MinDrops = 0;
        public int MaxDrops = 0;
        public int HP_YieldPerDrop = 0;
        public int EXP_YieldPerDrop = 0;

        /// <summary>
        /// Returns a AttackTiming with values corresponding to the given CreatureType
        /// </summary>
        /// <param name="_creatureType"></param>
        /// <returns></returns>
        public CreatureDrops(CreatureType _creatureType)
        {
            switch (_creatureType)
            {
                case CreatureType.Slime:
                    MinDrops = 0;
                    MaxDrops = 2;
                    HP_YieldPerDrop = 1;
                    EXP_YieldPerDrop = 1;
                    break;
                default:
                    Debug.LogError("Invalid CreatureType given, no CreatureDrops set for: (" + _creatureType.ToString() + ")");

                    MinDrops = 0;
                    MaxDrops = 0;
                    HP_YieldPerDrop = 0;
                    EXP_YieldPerDrop = 0;
                    break;
            }
        }
    }
}