﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Food : MonoBehaviour
{
    public int HealthYield = 2;
    public int ExpYield = 5;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Collider2D>().isTrigger = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>() != null)
        {
            if (PlayerController.Instance.IsFeeding)
            {
                PlayerController.Instance.EatFood(HealthYield, ExpYield);
                Destroy(gameObject);
            }
        }
    }
}
