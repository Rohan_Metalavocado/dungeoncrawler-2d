﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CreatureSystems
{
    public class Stats : MonoBehaviour
    {
        //The level of the character
        [Range(1, 100)]
        public int Level = 1;
        public int Experience = 0;

        //Base stat total (unmodified by creature type)
        public int BaseStatTotal = 0;

        public int CurrentHealth = 0;
        public int TotalHealth = 0;

        private int initialBaseStatTotal = 20; //Minimum number of stat points (this value at level 1)

        // Use this for initialization
        void Awake()
        {
            CalculateInitialStats();
        }

        /// <summary>
        /// Given the level of a created player/enemy, it will set the initial base and creature stats.
        /// </summary>
        /// <param name="_level"></param>
        public void CalculateInitialStats()
        {
            if (Level < 1)
            {
                Debug.LogError("Invalid Level number given to CalculateInitialStats. Must not be less than 1.");
                return;
            }

            BaseStatTotal = InitialBaseStat(initialBaseStatTotal);
        }

        //Calculates the initial base stat value for a stat, using the given initial value. (health, attack and defence don't start at 0 for level 1)
        int InitialBaseStat (int _initialLvl1Value)
        {
            return _initialLvl1Value + Mathf.FloorToInt(Random.Range((Level - 1) * 3, (Level - 1) * 4 + 1));
        }

        /// <summary>
        /// Adds health to the player's CurrentHealth, making sure it doesn't exceed the TotalHealth
        /// </summary>
        /// <param name="_healthToAdd"></param>
        public void AddHealth (int _healthToAdd)
        {
            CurrentHealth = Mathf.Clamp(CurrentHealth + _healthToAdd, 0, TotalHealth);

            //Check if the player has no health
            //Added in case there are food sources that damage the player
            if (CurrentHealth == 0)
            {
                //Death logic
                
            }
        }

        /// <summary>
        /// Adds experience to the character and increases level if required.
        /// </summary>
        /// <param name="_expToAdd"></param>
        public void AddExperience (int _expToAdd)
        {
            if (_expToAdd <= 0) return;

            Experience += _expToAdd;

            //exp formula is exp = pow(LvlNum, 3) + 5 * LvlNum - 6
            bool finalLevelFound = false;
            int abortCounter = 0;

            //Check the current experience level against the requirements for the next level and increase the level if required.
            while (!finalLevelFound)
            {
                int expForCurrentLevel = Mathf.FloorToInt(Mathf.Pow(Level, 3) + 5 * Level - 6);
                int expForNextLevel = Mathf.FloorToInt(Mathf.Pow(Level + 1, 3) + 5 * (Level + 1) - 6);

                int expDiff = Experience - expForCurrentLevel;

                if (expDiff >= (expForNextLevel - expForCurrentLevel))
                {
                    //LEVEL UP!!!!
                    LevelUp();  
                }
                else
                {
                    finalLevelFound = true;
                    break;
                }

                abortCounter++;

                if (abortCounter >= 100)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Increases the character's level and BaseStats
        /// </summary>
        void LevelUp ()
        {
            //TODO: Something cool when they level up
            Level++;

            //Increase stats
            int statTotalIncrease = Random.Range(3, 5);
            BaseStatTotal += statTotalIncrease;
        }

    }
}